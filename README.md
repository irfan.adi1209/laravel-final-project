<h1>Selamat Datang!</h1><br><br>
<h3>Web ini merupakan Final Project untuk menutup pelatihan web developer yang diselenggarakan oleh CDA IPB x SanberCode.</h3><br><br>
<h4>Silahkan melihat-lihat menu forum. Anda perlu log in / membuat akun untuk membuat, mengedit, dan menghapus semua fitur dalam forum kami.</h4><br>
<p>Forum ini dapat dijadikan menu tanya-jawab dalam komunitas tertentu.</p><br>
<h5>Fitur yang disediakan dalam web ini:</h5>
<ul>
    <li>Log-in dan Register</li>
    <li>Menambah, mengedit, dan menghapus posts</li>
    <li>Menambah, mengedit, dan menghapus replies</li>
    <li>Mengedit profil kalian sendiri</li>
    <li>Dapat memasukkan gambar ke dalam post maupun reply</li>
</ul><br><br>
<h5>Plug-in yang digunakan dalam project ini:</h5>
<ul>
    <li>AdminLTE versi 3</li>
    <li>CKEditor versi 4.6</li>
    <li>Laravel File Manager</li>
</ul>
<h5>ERD:</h5>

![](erd.PNG)

<br>
<h5>Collaborators:</h5>
<ul>
    <li>Muhammad Irfan Adi Prayoga</li>
    <li>Panji Wira Manggala</li>
    <li></li>
</ul>
<!--- Edit li di atas ^^^ -->

<h1>Quick Installation</h1>
Clone the repository

    git clone https://gitlab.com/irfan.adi1209/laravel-final-project.git

Switch to the repo folder

    cd laravel-final-project
    cd laravelFinalProject

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file
(APP_URL must be blank in order to use image feature in development server)

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

Run server in localhost

    localhost:8000

Happy explore this repo!
