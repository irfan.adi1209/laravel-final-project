@extends('layout.master')
@section('judul')
    Home Page
@endsection
@section('content')
    <h1>Selamat Datang!</h1><br><br>
    <h3>Web ini merupakan Final Project untuk menutup pelatihan web developer yang diselenggarakan oleh CDA IPB x SanberCode.</h3><br><br>
    <h4>Silahkan melihat-lihat menu forum. Anda perlu log in / membuat akun untuk membuat, mengedit, dan menghapus semua fitur dalam forum kami.</h4><br>
    <p>Forum ini dapat dijadikan menu tanya-jawab dalam komunitas tertentu.</p><br>
    <h5>Fitur yang disediakan dalam web ini:</h5>
    <ul>
        <li>Log-in dan Register</li>
        <li>Menambah, mengedit, dan menghapus posts</li>
        <li>Menambah, mengedit, dan menghapus replies</li>
        <li>Mengedit profil kalian sendiri</li>
        <li>Dapat memasukkan gambar ke dalam post maupun reply</li>
    </ul><br><br>
    <h5>Plug-in yang digunakan dalam project ini:</h5>
    <ul>
        <li>AdminLTE versi 3</li>
        <li>CKEditor versi 4.6</li>
        <li>Laravel File Manager</li>
    </ul>
@endsection