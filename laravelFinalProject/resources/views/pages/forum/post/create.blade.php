@extends('layout.master')

@section('judul')
    Tambah Post
@endsection

@section('content')
    <form action="/forum" method="POST">
        @csrf
        @method('post')

        <div class="form-group">
            <label for="pjudul">Judul</label>
            <input name="pjudul" type="text" class="form-control" id="pjudul">
        </div>
        @error('pjudul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label for="pcontent">Isi Post</label>
            <textarea name="pcontent" id="pcontent" rows="10" cols="80">
                {{-- kosong --}}
            </textarea>
        </div>
        @error('pcontent')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="kategori">Kategori</label>
            <input name="kategori" type="text" class="form-control" id="kategori">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Kirim</button>
    </form>
@endsection

@push('scripts')
    @include('ckeditor/setting')
    <script>
        CKEDITOR.replace('pcontent', options);
    </script>
@endpush