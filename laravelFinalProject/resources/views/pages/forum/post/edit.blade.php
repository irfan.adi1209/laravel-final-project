@extends('layout.master')
@section('judul')
    Edit Post
@endsection
@section('content')
@if($post->user_id == auth()->user()->id)
    <form action="/forum/{{$post->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="pjudul">Judul</label>
            <input name="pjudul" type="text" class="form-control" id="pjudul" value="{{$post->pjudul}}" disabled>
        </div>
        @error('pjudul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="pcontent">Isi Post</label>
            <textarea name="pcontent" id="pcontent" rows="10" cols="80">
                {{-- database pcontent --}}
                {{$post->pcontent}}
            </textarea>
        </div>
        @error('pcontent')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="kategori">Kategori</label>
            <input name="kategori" type="text" class="form-control" id="kategori" value="{{$post->kategori}}">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Kirim</button>
    </form>
@endif
@if($post->user_id != auth()->user()->id)
<h1>Anda bukan pembuat post.</h1>
@endif
@endsection

@push('scripts')
@include('ckeditor/setting')
<script>
    CKEDITOR.replace('pcontent', options);
</script>
@endpush