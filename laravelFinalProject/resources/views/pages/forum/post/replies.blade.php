@extends('layout.master')
@section('judul')
    {{-- judul --}}
    <a href="/forum">Forums</a> / {{$post->pjudul}}
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    Pengirim: {{-- database user --}} {{$post->author->name}}
  </div>
  <div class="card-body">
      <div class="row">
          <div class="col">
              <a href="/forum/{{$post->id}}">
                  <h1>
                      {{$post->pjudul}}
                  </h1>
              </a>
              <hr>
              {{-- database pcontent --}}
              {!!$post->pcontent!!}
          </div>
          @if($post->user_id == auth()->user()->id)
          <div class="col-0.5 mt-auto">
              {{-- edit delete --}}
              <form action="/forum/{{$post->id}}" method="post">
              @csrf
              @method('delete')
              <a href="/forum/{{$post->id}}/edit" class="btn btn-info mb-1" style="width: 4.25em">Edit</a>
              <br>
              <input class="btn btn-danger" type="submit" value="Hapus">
              </form>
          </div>
          @endif
      </div>
  </div>
  <div class="card-footer text-muted">
    {{-- database waktu (updated_at) --}}
    Last modified: {{$post->updated_at}}
  </div>
</div>

<br><br>

<h2>Replies:</h2>

@forelse ($post->ptreply as $key=>$item)
  <div class="card">
    <div class="card-header">
      Pengirim: {{-- database user --}} {{$item->commenter->name}}
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {{-- database pcontent --}}
                {!!$item->rcontent!!}
            </div>
            @if($item->user_id == auth()->user()->id))
            <div class="col-0.5 mt-auto">
                {{-- edit delete --}}
                <form action="/reply/{{$item->id}}" method="post">
                  @csrf
                  @method('delete')
                  <a href="/reply/{{$item->id}}/edit" class="btn btn-info mb-1" style="width: 4.25em">Edit</a>
                  <br>
                  <input class="btn btn-danger" type="submit" value="Hapus">
                </form>
            </div>
            @endif
        </div>
    </div>
    <div class="card-footer text-muted">
      {{-- database waktu (updated_at) --}}
      Last modified: {{$item->updated_at}}
    </div>
  </div>
@empty
  <h1>Tidak ada balasan.</h1>
  <br><br><br>
@endforelse

@auth
  <form action="/reply" method="post">
    @csrf
    @error('rcontent')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <input type="hidden" value="{{$post->id}}" name="post_id">
      <label for="rcontent">Tambah balasan</label>
      <textarea name="rcontent" id="rcontent" rows="10" cols="80">
          {{-- kosong --}}
      </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Kirim</button>
  </form>
@endauth

@guest
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Masuk untuk memberikan komentar!</h1>
    <p class="lead">Silahkan log-in menggunakan akun kalian untuk memberikan komentar.</p>
  </div>
</div>
@endguest
@endsection

@push('scripts')
@include('ckeditor/setting')
<script>
  CKEDITOR.replace('rcontent', options);
</script>
@endpush