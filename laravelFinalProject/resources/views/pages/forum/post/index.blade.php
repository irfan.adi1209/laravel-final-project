@extends('layout.master')
@section('judul')
    Forums
@endsection
@section('content')
@auth
<a href="/forum/create" class="btn-lg btn-primary">Tambah Post</a><br><br>
@endauth
@forelse ($posts as $key=>$item)
<div class="card">
    <div class="card-header">
      Pengirim: {{-- database user --}} {{$item->author->name}}
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <a href="/forum/{{$item->id}}">
                    <h1>
                        {{$item->pjudul}}
                    </h1>
                </a>
                {{-- <hr> --}}
                {{-- database pcontent --}}
                {{-- {!!$item->pcontent!!} --}}
            </div>
            @if($item->user_id == auth()->user()->id)
            <div class="col-0.5 mt-auto">
                {{-- edit delete --}}
                <form action="/forum/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <a href="/forum/{{$item->id}}/edit" class="btn btn-info mb-1" style="width: 4.25em">Edit</a>
                <br>
                <input class="btn btn-danger" type="submit" value="Hapus">
                </form>
            </div>
            @endif
        </div>
    </div>
    <div class="card-footer text-muted">
      {{-- database waktu (updated_at) --}}
      Last modified: {{$item->updated_at}}
    </div>
</div>
@empty
    Tidak ada post.
@endforelse
@endsection