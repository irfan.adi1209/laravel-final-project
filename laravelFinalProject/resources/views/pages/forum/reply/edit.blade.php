@extends('layout.master')
@section('judul')
    Edit Reply
@endsection
@section('content')
@if($reply->user_id == auth()->user()->id)
  <form action="/reply/{{$reply->id}}" method="POST">
    @csrf
    @method('put')
    @error('rcontent')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <input type="hidden" value="{{$reply->postingid->id}}" name="post_id">
      <label for="rcontent">Ubah balasan</label>
      <textarea name="rcontent" id="rcontent" rows="10" cols="80">
          {{-- kosong --}}
          {{$reply->rcontent}}
      </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Ubah</button>
  </form>
@endif
@if($reply->user_id != auth()->user()->id)
<h1>Anda bukan pembuat komentar.</h1>
@endif
@endsection

@push('scripts')
@include('ckeditor/setting')
<script>
  CKEDITOR.replace('rcontent', options);
</script>
@endpush