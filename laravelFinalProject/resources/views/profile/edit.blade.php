@extends('layout.master')

@section('judul')
Update Profile
@endsection

@section('content')

<div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$profile->users->name}}" class="form-control" name="name" disabled>
</div>

<div class="form-group">
    <label>Email</label>
    <input type="text" value="{{$profile->users->email}}" class="form-control" name="email" disabled>
</div>

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('put')


<div class="form-group">
    <label>Umur</label>
    <input type="number" value="{{$profile->umur}}" class="form-control" name="umur">
</div>
@error('umur')
    <div class="alert alert-danger"></div>
@enderror

<div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
</div>
@error('alamat')
    <div class="alert alert-danger"></div>
@enderror

<div class="form-group">
    <label>Biodata</label>
    <textarea name="biodata" class="form-control">{{$profile->biodata}}</textarea>
</div>
@error('bio')
    <div class="alert alert-danger"></div>
@enderror

<button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection