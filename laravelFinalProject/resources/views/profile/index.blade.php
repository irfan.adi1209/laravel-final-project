@extends('layout.master')

@section('judul')
Profile
@endsection

@section('content')

<a href="/profile/edit" class="btn btn-primary mb-2">Edit Data</a>

<div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$profile->users->name}}" class="form-control" name="name" disabled>
</div>

<div class="form-group">
    <label>Email</label>
    <input type="text" value="{{$profile->users->email}}" class="form-control" name="email" disabled>
</div>

<div class="form-group">
    <label>Umur</label>
    <input type="number" value="{{$profile->umur}}" class="form-control" name="umur" disabled>
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control" disabled>{{$profile->alamat}}</textarea>
</div>

<div class="form-group">
    <label>Biodata</label>
    <textarea name="biodata" class="form-control" disabled>{{$profile->biodata}}</textarea>
</div>

@endsection