<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', 'home1Controller@index');

//test
Route::get('/test', function (){
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    
    // Profile
    // Route::resource('profile', 'ProfileController')->only([
    //     'index', 'update'
    // ]);
    Route::get('/profile', 'ProfileController@index');
    Route::put('/profile/{profile_id}', 'ProfileController@update');
    Route::get('/profile/edit', 'ProfileController@edit');
    // Create
    Route::get('/forum/create', 'formPostController@create');
    // Saving data create
    Route::post('/forum', 'formPostController@store');
    // Update
    Route::get('/forum/{forum_id}/edit', 'formPostController@edit'); // Page for edit
    Route::put('/forum/{forum_id}', 'formPostController@update'); //Saving changes
    // Delete
    Route::delete('/forum/{forum_id}', 'formPostController@destroy'); // Deleting post
    // Route reply
    // Saving data reply
    Route::post('/reply', 'formReplyController@store');
    // Update
    Route::get('/reply/{reply_id}/edit', 'formReplyController@edit');
    Route::put('/reply/{reply_id}', 'formReplyController@update'); //Saving changes
    // Delete
    Route::delete('/reply/{reply_id}' , 'formReplyController@destroy');

});

// Forum
// Route post /forum
Route::get('/forum', 'formPostController@index');
// Read
Route::get('/forum/{forum_id}', 'formPostController@show'); // Detail button




Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

// Laravel filemanager
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});