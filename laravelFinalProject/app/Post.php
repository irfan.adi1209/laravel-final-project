<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";

    protected $fillable = ['pjudul', 'pcontent', 'kategori', 'user_id'];

    
    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function ptreply()
    {
        return $this->hasMany('App\Reply', 'post_id');
    }
}
