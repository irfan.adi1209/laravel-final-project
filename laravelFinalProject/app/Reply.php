<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = "reply";

    protected $fillable = ['rcontent', 'user_id', 'post_id'];

    public function commenter()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function postingid()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
