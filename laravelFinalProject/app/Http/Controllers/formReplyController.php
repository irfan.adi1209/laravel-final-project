<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reply;
use App\Post;

class formReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.forum.reply.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rcontent' => 'required'
        ],
        [
            'rcontent.required' => 'Reply harus diisi.'
        ]);

        $reply = new Reply;
 
        $reply->rcontent = $request->rcontent;
        $reply->user_id = Auth::id();
        $reply->post_id = $request->post_id;
 
        $reply->save();

        return redirect('/forum/'.$request->post_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reply = Reply::find($id);
        // dd($post);
        return view('pages.forum.reply.edit', compact('reply'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'rcontent' => 'required'
        ],
        [
            'rcontent.required' => 'Reply harus diisi.'
        ]);

        $reply = Reply::find($id);
        
        $reply->rcontent = $request->rcontent;
        
        $reply->save();
        
        // dd($reply);

        return redirect('/forum/'.$request->post_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = Reply::Where('id', $id);
        // dd($reply);
        $reply->delete();
        return redirect()->back();
    }
}
