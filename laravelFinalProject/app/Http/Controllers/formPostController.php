<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Reply;

class formPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all()->sortByDesc('updated_at');
        // dd($post);
        return view('pages.forum.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.forum.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pjudul' => 'required',
            'pcontent' => 'required',
            'kategori' => 'required'
        ],
        [
            'pjudul.required' => 'Judul harus diisi.',
            'pcontent.required' => 'Konten harus diisi.',
            'kategori.required' => 'Kategori harus diisi.'
        ]);

        $post = new Post;
 
        $post->pjudul = $request->pjudul;
        $post->pcontent = $request->pcontent;
        $post->kategori = $request->kategori;
        $post->user_id = Auth::user()->id;

        $post->save();

        return redirect('/forum/'.$post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $reply = Reply::all()->sortByDesc('updated_at');

        return view('pages.forum.post.replies', compact('post', 'reply'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        // dd($post);
        return view('pages.forum.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pcontent' => 'required',
            'kategori' => 'required'
        ],
        [
            'pcontent.required' => 'Konten harus diisi.',
            'kategori.required' => 'Kategori harus diisi.'
        ]);

        $post = Post::find($id);
 
        $post->pcontent = $request->pcontent;
        $post->kategori = $request->kategori;
 
        $post->save();

        return redirect('/forum/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/forum');
    }
}
